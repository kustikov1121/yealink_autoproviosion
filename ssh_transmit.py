import paramiko
import yaml


with open ('SECRETS.yaml', 'r') as creds:
    credentials = yaml.safe_load(creds)


def transmit(filename):
    ssh_client = paramiko.SSHClient()
    ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh_client.connect(hostname=credentials['hostname'],
                       username=credentials['username'],
                       password=credentials['pw'],
                       port=credentials['port'])
    ftp_client = ssh_client.open_sftp()
    #If its not dhcpd.conf so we are placing CONFIG file to tftp
    if filename != 'dhcpd.conf':
        ftp_client.put(f'mac_configs/{filename}', f'/srv/tftp/{filename}')
    #For dhcpd.conf we have to make backup file first and then, send the new one
    else:
        stdin, stdout, stderr = ssh_client.exec_command('sudo cp /etc/dhcp/dhcpd.conf /etc/dhcp/dhcpd.conf.bkp')
        stdin.write(credentials['pw'])
        ftp_client.put(f'config_files/{filename}', f'/etc/dhcp/{filename}')
        #Restarting dhcp daemon
        stdin, stdout, stderr = ssh_client.exec_command('sudo systemctl restart isc-dhcp-server.service')
        stdin.write(credentials['pw'])


    ftp_client.close()
    ssh_client.close()

