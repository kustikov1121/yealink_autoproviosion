def main(parameters):

    #Inserting specified parameters to every unique config file
    empty_file = []
    config_file = open('config_files/exmpl.exm', 'r')

    for pos, str in enumerate(config_file):
        numb, name, ip, mac= parameters
        if pos == 4:
            string = f'account.1.label = {numb}\n'
        elif pos == 6:
            string = f'account.1.display_name = {name}\n'
        elif pos == 8:
            string = f'account.1.auth_name = {numb}\n'
        elif pos == 11:
            string = f'account.1.user_name = {numb}\n'
        elif pos == 27:
            string = f'network.internet_port.ip = {ip}\n'
        else:
            string = str
        empty_file.append(string)

    with open(f'mac_configs/{mac}.cfg', 'w+') as f:
        for item in empty_file:
            f.write(item)



