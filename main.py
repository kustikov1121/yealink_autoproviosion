import time
import pandas as pd
import config_creator
import ssh_transmit
import dhcp_updater

iterration = 1
dhcp = []

file = 'dialplan.xlsx'
xl = pd.read_excel(file).values.tolist()

#print (type(xl))

#To create config file, its necessary to call Confgi creator with list of numb, name and id.
#name should be withot name and surname. Lastname only

#panded_data = pd.DataFrame(xl)
#panded_data.to_excel('dotted_mac.xlsx')

#creating MAC address in format necessary for dhcpd.conf
def dottify(undotted_mac):
    position = 0
    i = 0
    dotted_mac_list = []
    for n in list(str(undotted_mac)):
        dotted_mac_list.insert(position, n)
        position+=1
        i+=1
        if i == 2:
            dotted_mac_list.insert(position, ':')
            i=0
            position+=1
        dotted_mac = (''.join(dotted_mac_list))[:-1]

    return dotted_mac


for s in xl:
    mac = str(s[11])
    #if we dont have MAC for phone (any reasons) so we can scip that line
    if mac != str("nan"):
        print(f'Itteration #{iterration}')

        #input dotted MAC at 12 column
        mac_dotted = dottify(mac)
        s[12] = mac_dotted
        mac = mac.lower()

        print(f'Added dotted MAC: {s[12]}')

        #create config file
        internal_number = s[2]
        name = s[14]
        ip_address = s[10]
        config_creator.main([internal_number, name, ip_address, mac])
        print(f'Created config file for: {name}\n'
              f'IP: {ip_address}\n'
              f'Internal number: {internal_number}\n'
              f'File named: {mac}.cfg\n')
        #sending file via ssh
        ssh_transmit.transmit(f'{mac.lower()}.cfg')
        #creating dhcpd host
        dhcp_string = f'\n' \
                      f'\nhost {internal_number} {{\n' \
                      f'  hardware ethernet {mac_dotted};\n' \
                      f'  fixed-address {ip_address};\n' \
                      f'}}'
        #Adding host to full list that needs to be written to file later
        dhcp.append(dhcp_string)

        iterration+=1
        time.sleep(0.02)


#Updating dhcpd.conf file
dhcp_updater.upd(dhcp)
#sending dhcp to server
ssh_transmit.transmit('dhcpd.conf')


panded_data = pd.DataFrame(xl)
panded_data.to_excel(file)