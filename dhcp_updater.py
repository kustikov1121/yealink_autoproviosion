
#Reading dhcpd.conf file and editing from line 121, where we specifying the hosts by MAC address
with open ('config_files/dhcpd.conf', 'r') as config_file:
    cfg = config_file.readlines()


def upd(dhcp_hosts):
    cntr = 1
    new_dhcp = []

    for line in cfg:
        if cntr < 121:
            new_dhcp.append(line)
            cntr+=1
        else:
            break

    for n in dhcp_hosts:
        new_dhcp.append(n)

    new_dhcp_file = open('config_files/dhcpd.conf', 'w+')
    for l in new_dhcp:
        new_dhcp_file.write(l)
    new_dhcp_file.close()


